﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PointsManager : MonoBehaviour {

	[SerializeField] private Text points_text;
	[SerializeField] private Text time_text;
	private int points;

	[SerializeField] private RandomSpawn rs;

	// Use this for initialization
	void Start () {

		rs = GameObject.Find ("Funghi").GetComponent<RandomSpawn> ();
		points_text = GameObject.Find ("Points").GetComponent<Text> ();

		points = 0;
		points_text.text = "POINTS: " + points.ToString ();
	}

	// Update is called once per frame
	void Update () {

	}

	/// <summary>
	/// OnTriggerEnter is called when the Collider other enters the trigger.
	/// </summary>
	/// <param name="other">The other Collider involved in this collision.</param>
	void OnTriggerEnter (Collider other) {
		if (other.gameObject.CompareTag ("Mushroom")) {
			Debug.Log ("COLLISION WITH: " + other.name);
			CallDisableRender (other.name);
		}
	}

	[PunRPC]
	public void DisableRender (string mush) {
		GameObject mushroom = rs.Container[mush];
		mushroom.SetActive (false);
	}

	public void CallDisableRender (string mush) {
		PhotonView photonView = this.GetComponent<PhotonView> ();
		photonView.RPC ("DisableRender", PhotonTargets.AllBufferedViaServer, mush);
		points = points + 1;
		points_text.text = "POINTS: " + points.ToString ();
	}

}