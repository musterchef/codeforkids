﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class RandomSpawn : Photon.MonoBehaviour {
	public GameObject terrain;
	public Transform placeHolder;
	public int numberOfObjects; // number of objects to place
	private int currentObjects; // number of placed objects
	public GameObject objectToPlace; // GameObject to place
	private int terrainWidth; // terrain size (x)
	private int terrainLength; // terrain size (z)
	private int terrainPosX; // terrain position x
	private int terrainPosZ; // terrain position z
	// private Dictionary<string, GameObject> objectsPlaced;
	public Dictionary<string, GameObject> Container { get; set; }
	/// <summary>
	/// Awake is called when the script instance is being loaded.
	/// </summary>
	void Awake () { }

	void Start () {
		Mesh mesh = terrain.GetComponent<MeshFilter> ().mesh;
		Container = new Dictionary<string, GameObject> ();

		// // terrain size x
		// terrainWidth = (int) mesh.bounds.size.x;
		// // terrain size z
		// terrainLength = (int) mesh.bounds.size.z;
		// // terrain x position
		// terrainPosX = (int) placeHolder.transform.position.x;
		// // terrain z position
		// terrainPosZ = (int) placeHolder.transform.position.z;

		// terrain size x
		terrainWidth = 90;
		// terrain size z
		terrainLength = 190;
		// terrain x position
		terrainPosX = -50;
		// terrain z position
		terrainPosZ = -160;

	}
	// Update is called once per frame
	void Update () {

		if (Input.GetMouseButtonDown (0)) {
			Debug.Log ("Container.count = " + Container.Count);
		}

		if (PhotonNetwork.connectionStateDetailed.ToString () == "Joined") {
			// generate objects
			if (currentObjects <= numberOfObjects) {

				if (PhotonNetwork.isMasterClient) {

					// generate random x position
					int posx = Random.Range (terrainPosX, terrainPosX + terrainWidth);
					// generate random z position
					int posz = Random.Range (terrainPosZ, terrainPosZ + terrainLength);

					// // generate random x position
					// int posx = Random.Range (terrainPosX, terrainPosX + terrainWidth);
					// // generate random z position
					// int posz = Random.Range (terrainPosZ, terrainPosZ + terrainLength);

					// get the terrain height at the random position
					// float posy = Terrain.activeTerrain.SampleHeight (new Vector3 (posx, 0, posz));

					float posy;
					RaycastHit hit;
					if (Physics.Raycast (new Vector3 (posx, 100f, posz), transform.TransformDirection (Vector3.down), out hit, Mathf.Infinity)) {
						posy = 100f - hit.distance + 0.2f;
						// Debug.Log ("HIT");
					} else {
						posy = 0;
						// Debug.Log ("NOHIT");
					}

					// create new gameObject on random position
					// GameObject newObject = (GameObject) Instantiate (objectToPlace, new Vector3 (posx, posy, posz), Quaternion.identity);
					GameObject go;
					go = PhotonNetwork.InstantiateSceneObject ("objectToPlace", new Vector3 (posx, posy, posz), Quaternion.identity, 0, null);
					// go.transform.parent = this.transform;
					// Container.Add (go);
					currentObjects += 1;
				}
				if (currentObjects == numberOfObjects) {
					Debug.Log ("Generate objects complete!");
				}
			}
		}
	}

	// var hit : RaycastHit;
	// var heightAboveGround = 0;

	// if (Physics.Raycast (transform.position, transform.TransformDirection (Vector3.down) , hit, Mathf.Infinity))
	// {
	//     heightAboveGround = hit.distance;
	// }

}