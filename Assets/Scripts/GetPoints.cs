﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GetPoints : MonoBehaviour {

	private FunghiManager funghiManager;

	// Use this for initialization
	void Start () {
		funghiManager = GameObject.Find ("Funghi").GetComponent<FunghiManager> ();
	}

	// Update is called once per frame
	void Update () {

	}

	/// <summary>
	/// OnTriggerEnter is called when the Collider other enters the trigger.
	/// </summary>
	/// <param name="other">The other Collider involved in this collision.</param>
	void OnTriggerEnter (Collider other) {
		CallDisableRender ();
	}

	[PunRPC]
	public void DisableRender () {
		this.gameObject.SetActive (false);
	}

	public void CallDisableRender () {
		PhotonView photonView = PhotonView.Get (this);
		// photonView.RPC ("Activate3DPlayerOnClients", PhotonTargets.All);
		photonView.RPC ("DisableRender", PhotonTargets.AllBufferedViaServer);
		// ActivateAndDisableMesh ();
	}

}