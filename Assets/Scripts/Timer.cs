﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour {

	private float tempo;
	[SerializeField] private Text testo;
	private float cronometro;

	// Use this for initialization
	void Start () {
		tempo = 0;
		cronometro = 60f;
	}
	
	// Update is called once per frame
	void Update () {
		tempo = tempo + Time.deltaTime;
		cronometro = cronometro - tempo ;
		// cronometro = cronometro - Time.deltaTime;
		testo.text = "TIME REMAINING: " + ((int) cronometro).ToString();
	}
}
