﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FunghiManager : MonoBehaviour {

	private int punti;
	[SerializeField] private Text pun;

	// Use this for initialization
	void Start () {
		punti = 0;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void IncrementPunti(){
		punti = punti + 1;
		pun.text = "POINTS: " + punti.ToString();
	}
}
