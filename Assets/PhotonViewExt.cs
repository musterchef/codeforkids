﻿using System.Collections;
using System.Collections.Generic;
using Photon;
using UnityEngine;

public class PhotonViewExt : PunBehaviour {

	/// <summary>
	/// Start is called on the frame when a script is enabled just before
	/// any of the Update methods is called the first time.
	/// </summary>
	void Awake () {

	}

	public override void OnPhotonInstantiate (PhotonMessageInfo info) {
		RandomSpawn rs = FindObjectOfType<RandomSpawn> ();
		Dictionary<string,GameObject> c = rs.Container;
		this.name = "MUSH_" + c.Count.ToString ();
		this.transform.parent = rs.gameObject.transform;
		c.Add (this.name, this.gameObject);

	}

}