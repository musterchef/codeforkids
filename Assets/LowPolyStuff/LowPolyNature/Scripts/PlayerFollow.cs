﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerFollow : MonoBehaviour {

    public Transform PlayerTransform;

    private Vector3 _cameraOffset;

    [Range(0.01f, 1.0f)]
    public float SmoothFactor = 0.5f;

    public bool LookAtPlayer = false;

    public bool RotateAroundPlayer = true;

    public float RotationsSpeed = 5.0f;

	// Use this for initialization
	void Start () {
        _cameraOffset = transform.position - PlayerTransform.position;	
	}
	
	// LateUpdate is called after Update methods
	void LateUpdate () {

        if(RotateAroundPlayer)
        {
            Vector3 dir = (PlayerTransform.position - this.transform.position).normalized;
            Quaternion camTurnAngle = Quaternion.LookRotation(dir);
            Quaternion curRot = Quaternion.Slerp(this.gameObject.transform.rotation, camTurnAngle, Time.deltaTime * 2f);
            //Quaternion.AngleAxis(Input.GetAxis("Mouse X") * RotationsSpeed, Vector3.up);
            this.transform.rotation = curRot;
            // _cameraOffset = camTurnAngle * _cameraOffset;
            // _cameraOffset = curRot * _cameraOffset;
        }

        Vector3 newPos = PlayerTransform.position;

         transform.position = Vector3.Slerp(transform.position, newPos, SmoothFactor);

        if (LookAtPlayer || RotateAroundPlayer)
            transform.LookAt(PlayerTransform);
	}
}
